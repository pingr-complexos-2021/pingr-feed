import getTables from "./getTables";
import createTable from "./createTable";
import getTableById from "./getTableById";
import getTablePings from "./getTablePings";

export { getTables, getTablePings, getTableById, createTable };
