import { Request, Response } from "express";

import { IUser } from "src/types";
import { Models } from "src/models";

const getTablePings = async (req: Request, res: Response): Promise<void> => {
  const { PingTable, User, Table }: Models = req.app.locals.models;
  const userId = req.params.userId;
  const tableId = req.params.tableId;

  const pings = await PingTable.find({ userId, tableId }).exec();
  const userIds: string[] = [...new Set(pings.map((ping) => ping.userId))];
  const users: Record<string, IUser> = {};

  await Promise.all(
    userIds.map(async (userId) => {
      const user = await User.findById(userId).exec();
      if (user) {
        users[userId] = user;
      }
    })
  );

  const table = {
    pings: pings.map(({ userId, _id, text, tags, imageURLs, date }) => {
      const user = users[userId];
      return { id: _id, body: text, media: imageURLs, tags, date, user };
    }),
  };

  res.status(200).json({ table });
};

export default getTablePings;
