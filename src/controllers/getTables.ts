const getTables = async (req: any, res: any): Promise<void> => {
  const { Table } = req.app.locals.models;
  const userId = req.params.userId;
  try {
    const tables = await Table.find({ userId }).exec();
    res.status(200).send({ tables });
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal server error");
  }
};

export default getTables;
