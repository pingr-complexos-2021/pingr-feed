const getTableById = async (req: any, res: any): Promise<void> => {
  const { Table } = req.app.locals.models;
  const tableId = req.params.tableId;
  try {
    const table = await Table.findById(tableId).exec();
    if (table === null) {
      res.status(404).send("Table not found")
      return
    }

    res.status(200).send({ table });
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal server error");
  }
};

export default getTableById;
