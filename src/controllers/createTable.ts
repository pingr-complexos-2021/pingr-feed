const createTable = async (req: any, res: any): Promise<void> => {
  const { Table } = req.app.locals.models;
  const table = new Table({
    userId: req.params.userId,
    ...req.body,
  });

  try {
    const result = await Table.create(table);
    res.status(200).send(result);
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal server error");
  }
};

export default createTable;
