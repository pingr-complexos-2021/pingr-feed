import cors from "cors";
import express from "express";

import { Table, User, Ping, PingTable } from "./models";
import { getTables, getTablePings, getTableById, createTable } from "./controllers";

export default (corsOptions: any, { stanConn, mongoClient, secret, }: any) => {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.locals.models = { Table, User, Ping, PingTable };

  api.get("/", (_, res) => res.json("Hello, World!"));

  api.get("/users/:userId/tables", getTables);

  api.get("/users/:userId/tables/:tableId/pings", getTablePings);

  api.get("/users/:userId/tables/:tableId", getTableById);

  api.post("/users/:userId/tables", createTable);

  return api;
};
