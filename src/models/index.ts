import mongoose, { Model } from "mongoose";

import { Ping } from './Ping';
import { User } from './User';
import { Table } from './Table';
import { PingTable } from './PingTable';
import { ITable, IUser, IPing, IPingTable } from "src/types";

interface Models {
  Table: Model<ITable>;
  User: Model<IUser>;
  Ping: Model<IPing>;
  PingTable: Model<IPingTable>;
}

const initDB = (url: string) => {
  mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
}

export { initDB, Models, Table, User, Ping, PingTable };
