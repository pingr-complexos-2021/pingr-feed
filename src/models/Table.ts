import { Schema, model, Model } from "mongoose";
import { ITable } from "../types/Table.types";

const TableSchema = new Schema({
  userId: String,
  title: String,
  usersFilter: [String],
  hashtagFilter: [String]
});

export const Table: Model<ITable> = model('Table', TableSchema);
