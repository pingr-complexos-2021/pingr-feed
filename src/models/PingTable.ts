import { Schema, model, Model } from "mongoose";
import { IPingTable } from "../types/PingTable.types";

const PingTableSchema = new Schema({
  tableId: String,
  parentId: String,
  userId: String,
  text: String,
  tags: [String],
  imageURLs: [String],
  visibilityOverride: String,
  date: Date,
});

export const PingTable: Model<IPingTable> = model("PingTable", PingTableSchema);
