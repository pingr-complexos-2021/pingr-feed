import { Schema, model, Model } from "mongoose";
import { IUser } from "../types/User.types";

const UserSchema = new Schema({
  username: String,
  email: String,
});

export const User: Model<IUser> = model('User', UserSchema);
