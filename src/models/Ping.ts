import { Schema, model, Model } from "mongoose";
import { IPing } from "../types/Ping.types";

const PingSchema = new Schema({
  parentId: String,
  userId: String,
  text: String,
  tags: [String],
  imageURLs: [String],
  visibilityOverride: String,
  date: Date,
});

export const Ping: Model<IPing> = model("Ping", PingSchema);
