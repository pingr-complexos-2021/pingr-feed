import stan from "node-nats-streaming";
import { PingTable, Table } from "./models";

export default function (mongoClient: any) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");
    const opts = conn.subscriptionOptions().setStartWithLastReceived();
    const subscription = conn.subscribe("Ping", opts);

    subscription.on("message", async (msg) => {
      let event = JSON.parse(msg.getData());
      console.log("Received", event);
      if (event.eventType === "PingCreated") {
        const data = event.entityAggregate;
        const tables = await Table.find({ usersFilter: data.userId }).exec();
        tables.forEach(({ _id: tableId }) => {
          const ping = PingTable.create({ ...data, tableId }).then((res) => {
            console.log(`Created ping ${res}`);
          });
        });
      }
    });
  });

  return conn;
}
