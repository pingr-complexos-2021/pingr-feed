import { Document } from "mongoose";

export interface ITable extends Document {
  userId: string,
  title: string,
  usersFilter: string[],
  hashtagFilter: string[]
}
