import { IPing } from "./Ping.types";

export interface IPingTable extends IPing {
  tableId: string,
}
