import { Document } from "mongoose";

const enum PingVisibility {
  public = "public",
  private = "private",
  restricted = "restricted",
}

export interface IPing extends Document {
  id: string;
  parentId?: string;
  userId: string;
  text: string;
  tags: string[];
  imageURLs: string[];
  visibilityOverride?: PingVisibility;
  date: Date;
}
