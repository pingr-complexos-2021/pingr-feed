/*                               */
/* YOU MUST NOT CHANGE THIS FILE */
/*                               */

import API from "./api";
import BrokerClient from "./nats";
import mongoClient from "./mongo";
import { initDB } from "./models";

const PORT = process.env.API_PORT;
const ALLOWED_ORIGINS = JSON.parse(process.env.API_ALLOWED_ORIGINS || "");

const secret = process.env.API_SECRET;

const corsOptions = {
  origin: function (origin: any, callback: any) {
    if (ALLOWED_ORIGINS.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
};

async function main() {
  try {
    await mongoClient.connect();
    console.log("Connected to MongoDB");

    initDB(process.env.MONGO_URL || "");
    console.log("Connected to MongoDB");

    const stanConn = BrokerClient(mongoClient);

    const context = { mongoClient, stanConn, secret };

    const api = API(corsOptions, context);
    api.listen(PORT, () =>
      console.log(`Listening at http://localhost:${PORT}`)
    );
  } catch (e) {
    console.dir(e);
  }
}

main();

/*                               */
/* YOU MUST NOT CHANGE THIS FILE */
/*                               */
