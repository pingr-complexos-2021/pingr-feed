import { MongoClient } from "mongodb";

const connectionURL = process.env.MONGO_URL || "mongodb://127.0.0.1:27017";
console.log(`Connection URL: ${connectionURL}`);
export default new MongoClient(connectionURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
