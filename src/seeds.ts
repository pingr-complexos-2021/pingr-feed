import { PingTable, Table, User } from "./models";

async function bootstrap() {
  const user = await User.create({
    username: "bob",
    email: "bob@bob.com",
  });

  console.log(`User id: ${user._id}`);

  const table = await Table.create({
    userId: user._id,
    title: "Titulo",
    usersFilter: [],
    hashtagFilter: [],
  });
  await PingTable.create({
    tableId: table._id,
    parentId: null,
    userId: user._id,
    text: "Meu ping",
    tags: [],
    imageURLs: [],
    visibilityOverride: "public",
    date: new Date(),
  });
}

export default bootstrap;
