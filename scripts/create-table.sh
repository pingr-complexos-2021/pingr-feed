set -x
curl 127.0.0.1:3000/users/123/tables -i \
  -H 'content-type: application/json' \
  -d '{
  "title": "Tabela Dahora",
  "usersFilter": [
    "60bd4bfcb65a660a9604d025",
    "rodorgas",
    "binivov",
    "drolean"
  ],
  "hashtagFilter": [
    "tequinologia",
    "wumpus"
  ]
}'
