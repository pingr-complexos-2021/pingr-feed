import mongoClient from "../src/mongo";
import stan from "node-nats-streaming";

const publish = async () => {
  await mongoClient.connect();
  const event = JSON.stringify({
    eventType: "PingCreated",
    entityAggregate: {
      parentId: null,
      userId: "60bd4bfcb65a660a9604d025",
      text: "olá mundo!",
      tags: [],
      imageURLs: [],
      visibilityOverride: "public",
      date: "2021-06-06T21:19:57.505Z",
    },
  });
  const stanConn = stan.connect("test-cluster", "publishTest", {
    url: process.env.BROKER_URL || "nats://127.0.0.1:4222",
  });
  stanConn.on("connect", () => {
    stanConn.publish("Ping", event);
    stanConn.close();
  });
};

publish();
